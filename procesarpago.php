
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <title>Inscripción</title>
  </head>
  <body>


    <section> 
        <div class="logo">
            <a href="#"><img src="img/logo_CE.png"></a> 
        </div>
    </section>
    <section class="container-contact100">
        <div class="wrap-contact100">
            <h3 class="text-uppercase text-center">Resultado Operación</h3>

  <?php
  require_once 'vendor/autoload.php';
  include 'db_connect.php';

  //prepara para realizar el pago
  $token = $_REQUEST["token"];
  $payment_method_id = $_REQUEST["payment_method_id"];
  $installments = $_REQUEST["installments"];
  $issuer_id = $_REQUEST["issuer_id"];

  //de produccion de crea.
  MercadoPago\SDK::setAccessToken("TEST-7648669744248285-050511-d256bca2583c3b1fd9347aab9a44ee49__LD_LB__-254949937");
                                      

//  MercadoPago\SDK::setAccessToken("TEST-5237695078137883-021415-38362c024744c71cd2daa567da34d93a-74430786");

  $payment = new MercadoPago\Payment();
  $payment->transaction_amount = 600;
  $payment->token = $token;
  $payment->description = "Inscripción Congreso Educación";
  $payment->installments = $installments;
  $payment->payment_method_id = $payment_method_id;
  $payment->issuer_id = $issuer_id;
  $payment->payer = array(
  "email" => $_POST['correo2'] //creo que es el mail del que va a pagar
    );
  // Guarda y postea el pago
  $respuesta = $payment->save();

  $nombre                       = mysqli_real_escape_string($conn, $_POST['nombre2']);
  $apellido                     = mysqli_real_escape_string($conn, $_POST['apellido2']);
  $edad                         = mysqli_real_escape_string($conn, $_POST['edad2']);
  $documento                    = mysqli_real_escape_string($conn, $_POST['documento2']);
  $telefono                     = mysqli_real_escape_string($conn, $_POST['telefono2']);
  $correo                       = mysqli_real_escape_string($conn, $_POST['correo2']);
  $nivel                        = mysqli_real_escape_string($conn, $_POST['nivel2']);
  $institucion                  = mysqli_real_escape_string($conn, $_POST['institucion2']);
  $localidad                    = mysqli_real_escape_string($conn, $_POST['localidad2']);
  $provincia                    = mysqli_real_escape_string($conn, $_POST['provincia2']);
  $cargo                        = mysqli_real_escape_string($conn, $_POST['cargo2']);

  $estado                       = $payment->status;
  $estadoDetalle                = $payment->status_detail;
  $transaccionId                = $payment->id;
  $transaccionFecha             = $payment->date_approved;
  $pagador                      = strval( var_export( $payment->payer, true ) );
  $pagador                      = str_replace("'", "\'", $pagador);
  $tipoPago                     = $payment->payment_type_id;
  $metodoPago                   = $payment->payment_method_id;

  $pagadorId                    = $payment->payer->id;
  $pagadorEntidadTipo           = $payment->payer->entity_type;
  $pagadorTipo                  = $payment->payer->type;
  $pagadorFirstName             = $payment->payer->first_name;
  $pagadorLastName              = $payment->payer->last_name;
  $pagadorEmail                 = $payment->payer->email;
  $pagadorFechaCreacion         = $payment->payer->date_created;
  $pagadorPhoneArea             = $payment->payer->phone->area_code;
  $pagadorPhoneNumber           = $payment->payer->phone->number;
  $pagadorPhoneExtension        = $payment->payer->phone->extension;
  $pagadorIdentificacionNumber  = $payment->payer->identification->number;
  $pagadorIdentificacionTipo    = $payment->payer->identification->type;
  $pagadorIdentificacionAddress = $payment->payer->identification->address;
  $pagadorIdentificacionLast    = $payment->payer->identification->_last;

  $respuesta = strval( var_export($respuesta, true) );
  $respuesta = str_replace("'", "\'", $respuesta);

  
  if ( $payment -> status == "approved" ){

    $sql  = "INSERT INTO Inscriptos(Nombre, Apellido, Edad, Documento, Telefono, Mail, ";
    $sql .= "Nivel, Institucion, Localidad, Provincia, Cargo, Estado, Estado_Detalle, Transaccion_Id, ";
    $sql .= "Transaccion_Fecha, Pagador, Tipo_Pago, Metodo_Pago, Pagador_Id, Pagador_Entity_Type, ";
    $sql .= "Pagador_Type, Pagador_First_Name, Pagador_Last_Name, Pagador_Email, Pagador_Date_Created, ";
    $sql .= "Pagador_Phone_Area, Pagador_Phone_Number, Pagador_Phone_Extension, Pagador_Identification_Number,";
    $sql .= "Pagador_Identification_Type, Pagador_Identification_Address, Pagador_Identification_Last, Respuesta ) VAlUES ('";
    $sql .= $nombre . "', '" . $apellido . "', " . $edad . ", '" . $documento . "', '";
    $sql .= $telefono . "', '" . $correo . "', '" . $nivel . "', '" . $institucion . "' ,'";
    $sql .= $localidad . "', '" . $provincia . "', '" . $cargo . "', '" . $estado . "', '" ;
    $sql .= $estadoDetalle . "', '" . $transaccionId . "', '" . $transaccionFecha . "', '";
    $sql .= $pagador . "', '" . $tipoPago . "', '" . $metodoPago . "','" . $pagadorId . "', '";
    $sql .= $pagadorEntidadTipo . "', '" . $pagadorTipo . "', '" . $pagadorFirstName . "', '";
    $sql .= $pagadorLastName . "', '" . $pagadorEmail . "', '" . $pagadorFechaCreacion . "', '";
    $sql .= $pagadorPhoneArea . "', '" . $pagadorPhoneNumber . "', '" . $pagadorPhoneExtension . "','";
    $sql .= $pagadorIdentificacionNumber . "', '" . $pagadorIdentificacionTipo . "', '";
    $sql .= $pagadorIdentificacionAddress . "', '" . $pagadorIdentificacionLast . "', '" . $respuesta . "')";
    
    ?> 
   
      <h4 class="text-center"> Muchas gracias por inscribirte al VI Congreso de Educacion CREA Oeste</h4>
      <h5 class="text-center">Te esperamo el 1 de Junio en Nueve de Julio</h5>
      <p class="lead text-center">Número operación: <?php echo $transaccionId ?></p>

    <?php
    // if(mysqli_query($conn, $sql)) {
    //   echo "Se guardaron los datos";
    // } else {
    //   echo "Error en guardao de datos...Por favor intente de nuevo!";
    // }
    mysqli_query($conn, $sql);


  }else {

    /*
    echo $payment->status;
    echo "<br>";
    echo $payment->status_detail;
    */

    $sql  = "INSERT INTO errores(Nombre, Apellido, Edad, Documento, Telefono, Mail, ";
    $sql .= "Nivel, Institucion, Localidad, Provincia, Cargo, Estado, Estado_Detalle, Transaccion_Id, ";
    $sql .= "Transaccion_Fecha, Pagador, Tipo_Pago, Metodo_Pago, Pagador_Id, Pagador_Entity_Type, ";
    $sql .= "Pagador_Type, Pagador_First_Name, Pagador_Last_Name, Pagador_Email, Pagador_Date_Created, ";
    $sql .= "Pagador_Phone_Area, Pagador_Phone_Number, Pagador_Phone_Extension, Pagador_Identification_Number,";
    $sql .= "Pagador_Identification_Type, Pagador_Identification_Address, Pagador_Identification_Last, Respuesta ) VAlUES ('";
    $sql .= $nombre . "', '" . $apellido . "', " . $edad . ", '" . $documento . "', '";
    $sql .= $telefono . "', '" . $correo . "', '" . $nivel . "', '" . $institucion . "' ,'";
    $sql .= $localidad . "', '" . $provincia . "', '" . $cargo . "', '" . $estado . "', '" ;
    $sql .= $estadoDetalle . "', '" . $transaccionId . "', '" . $transaccionFecha . "', '";
    $sql .= $pagador . "', '" . $tipoPago . "', '" . $metodoPago . "','" . $pagadorId . "', '";
    $sql .= $pagadorEntidadTipo . "', '" . $pagadorTipo . "', '" . $pagadorFirstName . "', '";
    $sql .= $pagadorLastName . "', '" . $pagadorEmail . "', '" . $pagadorFechaCreacion . "', '";
    $sql .= $pagadorPhoneArea . "', '" . $pagadorPhoneNumber . "', '" . $pagadorPhoneExtension . "','";
    $sql .= $pagadorIdentificacionNumber . "', '" . $pagadorIdentificacionTipo . "', '";
    $sql .= $pagadorIdentificacionAddress . "', '" . $pagadorIdentificacionLast . "', '" . $respuesta . "')";

    mysqli_query($conn, $sql);
    ?>
      <p class="lead text-center">Lamentablemente no has finalizado tu inscripción. Intentalo nuevamente.</p>
      <br><hr><br>

      <input type="button" name="reintentar" id="reintentar" value="Reintentar" class="btn btn-success">

  <?php }


?>

        </div>
    </section>

    <section> 
        <div class="logo">
            <a href="#"><img src="img/pie.png"></a> 
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/funciones.js"></script>
  </body>
</html>
