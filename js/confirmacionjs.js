
//cargo los componentes del form con las variables definidas en localstorage
document.getElementById("nombre2").value      = localStorage.getItem('nombre');
document.getElementById("apellido2").value    = localStorage.getItem('apellido');
document.getElementById("edad2").value        = localStorage.getItem('edad');
document.getElementById("documento2").value   = localStorage.getItem('documento');
document.getElementById("telefono2").value    = localStorage.getItem('telefono');
document.getElementById("correo2").value      = localStorage.getItem('correo');
document.getElementById("nivel2").value       = localStorage.getItem('nivel');
document.getElementById("institucion2").value = localStorage.getItem('institucion');
document.getElementById("localidad2").value   = localStorage.getItem('localidad');
document.getElementById("provincia2").value   = localStorage.getItem('provincia');
document.getElementById("cargo2").value       = localStorage.getItem('cargo');

var anterior = document.getElementById("anterior");

anterior.addEventListener('click', function(e){
  //vuelve atras, al index.html
  history.back();


})
