  var formulario = document.getElementById('formulario');

  formulario.addEventListener('submit', function(e){
    //tomo datos formulario index.js y lo meto en localstorage
    var datos = new FormData(formulario);
    localStorage.setItem('nombre'     , datos.get('nombre'));
    localStorage.setItem('apellido'   , datos.get('apellido'));
    localStorage.setItem('edad'       , datos.get('edad'));
    localStorage.setItem('documento'  , datos.get('documento'));
    localStorage.setItem('telefono'   , datos.get('telefono'));
    localStorage.setItem('correo'     , datos.get('correo'));
    localStorage.setItem('nivel'      , datos.get('nivel'));
    localStorage.setItem('institucion', datos.get('institucion'));
    localStorage.setItem('localidad'  , datos.get('localidad'));
    localStorage.setItem('provincia'  , datos.get('provincia'));
    localStorage.setItem('cargo'      , datos.get('cargo'));

  })
